local paths = {}

paths.install_prefix = [[/home/ubuntu/torch/install]]
paths.install_bin = [[/home/ubuntu/torch/install/bin]]
paths.install_man = [[/home/ubuntu/torch/install/share/man]]
paths.install_lib = [[/home/ubuntu/torch/install/lib]]
paths.install_share = [[/home/ubuntu/torch/install/share]]
paths.install_include = [[/home/ubuntu/torch/install/include]]
paths.install_cmake = [[/home/ubuntu/torch/install/share/cmake/torch]]

return paths
